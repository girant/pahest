package com.artipd.tigran.pahest.Data;

import android.provider.BaseColumns;

/**
 * Created by tigran on 11/26/17.
 */

public final class DbItems {
    public DbItems(){

    };

    public static final class DbItemsNames implements BaseColumns{
        public final static String TABLE_NAME = "pahest";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_CODE = "code";
        public static final String COLUMN_COUNT = "count";
        public static final String COLUMN_COAST = "coast";
        public static final String COLUMN_LIMIT = "limit1";
    }

}

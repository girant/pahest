package com.artipd.tigran.pahest.Data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by tigran on 11/26/17.
 */

public class DataBase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "pahest.db";

    private static final int DATABASE_VERSION = 2;

    public DataBase (Context context){
        super (context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase dbPahest) {
        String SQL_CREATE_PAHEST_TABLE = "CREATE TABLE " + DbItems.DbItemsNames.TABLE_NAME + " ("
                + DbItems.DbItemsNames._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DbItems.DbItemsNames.COLUMN_NAME + " TEXT NOT NULL, "
                + DbItems.DbItemsNames.COLUMN_CODE + " TEXT NOT NULL, "
                + DbItems.DbItemsNames.COLUMN_COUNT + " INTEGER NOT NULL DEFAULT 0, "
                + DbItems.DbItemsNames.COLUMN_COAST + " DOUBLE NOT NULL DEFAULT 0, "
                + DbItems.DbItemsNames.COLUMN_LIMIT + " INTEGER NOT NULL DEFAULT 0);";
        dbPahest.execSQL(SQL_CREATE_PAHEST_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase dbPahest, int i, int i1) {
        if(i<i1) {
            dbPahest.execSQL("ALTER TABLE " + DbItems.DbItemsNames.TABLE_NAME +
                    " ADD COLUMN " + DbItems.DbItemsNames.COLUMN_LIMIT+" INTEGER NOT NULL DEFAULT 0;");
        }
    }
}

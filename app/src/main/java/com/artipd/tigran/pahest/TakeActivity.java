package com.artipd.tigran.pahest;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.artipd.tigran.pahest.Data.DataBase;
import com.artipd.tigran.pahest.Data.DbItems;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class TakeActivity extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {


    DataBase dbHelper;
    EditText etTakeName, etTakeCode, etTakeCount;
    Button btnTake, btnScanCode;
    Cursor cursor, cursChecker;
    SQLiteDatabase dbTake;
    static String[] projection;
    String strCode,isThere ;
    TextView tvArka;

    public static final String tableName = DbItems.DbItemsNames.TABLE_NAME;
    public static final String codeColumnName = DbItems.DbItemsNames.COLUMN_CODE;
    public static final String nameColnumName = DbItems.DbItemsNames.COLUMN_NAME;
    public static final String countColumnName = DbItems.DbItemsNames.COLUMN_COUNT;
    boolean isFromListView;
    String strGettedCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take);
        strGettedCode="";
        if(getIntent().getExtras() == null){
            isFromListView = false;
        }
        else {
            strGettedCode = getIntent().getExtras().getString("CodeFromBase");
            isFromListView = true;
            setTheme(android.R.style.Theme_DeviceDefault_Dialog);
        }


        tvArka =  findViewById(R.id.tvArka);
        strCode = DbItems.DbItemsNames.COLUMN_CODE;

        projection = new String[] {DbItems.DbItemsNames._ID, DbItems.DbItemsNames.COLUMN_CODE, DbItems.DbItemsNames.COLUMN_COUNT, DbItems.DbItemsNames.COLUMN_NAME, DbItems.DbItemsNames.COLUMN_COAST};
        dbHelper = new DataBase(this);
        dbTake = dbHelper.getWritableDatabase();
        etTakeName =  findViewById(R.id.editText_take_name);
        etTakeCode =  findViewById(R.id.editText_take_code);
        etTakeCount = findViewById(R.id.editText_take_count);
        btnTake =  findViewById(R.id.buttonTake);
        btnScanCode = findViewById(R.id.btn_scan_code);
        btnScanCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new IntentIntegrator(TakeActivity.this).setOrientationLocked(false).setCaptureActivity(CustomScannerActivity.class).initiateScan();
            }
        });
        if(!strGettedCode.equals("")){
            etTakeCode.setText(strGettedCode);
        }
        //etTakeCode.setOnFocusChangeListener(this);
        //ete shtrixkod@ sxal e scan exel apa anhrajesht e ayn popoxeluc heto
        //krkin setNames@ kanchel
        etTakeCount.setOnFocusChangeListener(this);

        btnTake.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        String strCode = DbItems.DbItemsNames.COLUMN_CODE;
        String strCodeCompare = etTakeCode.getText().toString();
        int toBase= 0;
        int countInBase ;

        ContentValues cv = new ContentValues();
        cursor = dbTake.query(DbItems.DbItemsNames.TABLE_NAME, projection, strCode + "=?", new String[]{strCodeCompare},null,null,null);
        while (cursor.moveToNext()) {
            int codeColnumIndex = cursor.getColumnIndex(DbItems.DbItemsNames.COLUMN_COUNT);
            countInBase = cursor.getInt(codeColnumIndex);
            toBase = countInBase - Integer.valueOf(etTakeCount.getText().toString());
        }


        if(toBase>=0){
            if(toBase==0){
                Snackbar.make(findViewById(R.id.activityTakeId),getString(R.string.countItemsNull), Snackbar.LENGTH_LONG ).show();
            }
            cv.put(DbItems.DbItemsNames.COLUMN_COUNT, toBase);
            dbTake.update(DbItems.DbItemsNames.TABLE_NAME, cv,strCode +"=" + strCodeCompare, null);
            clearViews();
        }
        else
        {
            Snackbar.make(findViewById(R.id.activityTakeId), getString(R.string.haventEnought), Snackbar.LENGTH_LONG).show();
          //  Toast.makeText(this, "You haven't enought items", Toast.LENGTH_LONG).show();
        }
        cursor.close();

    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_CANCELED){
            Toast.makeText(this, R.string.strNothingScanned, Toast.LENGTH_LONG).show();
        }
        else{
            if(scanningResult!=null){
                etTakeCode.setText(scanningResult.getContents().toString());
                setNames();
                etTakeCount.requestFocus();
            }
        }


    }
    public void setNames(){
        String codeFromEditText = etTakeCode.getText().toString();
        if(checkInBase(codeFromEditText)){
            Cursor cursChecker = dbTake.query(ActivityAdd.tableName, projection, DbItems.DbItemsNames.COLUMN_CODE + "=?", new String[] {codeFromEditText},null ,null, null, null);
            while(cursChecker.moveToNext()){
                int nameColnumIndex = cursChecker.getColumnIndex(nameColnumName);
                String nameInBase = cursChecker.getString(nameColnumIndex);
                int codeColnumIndex = cursChecker.getColumnIndex(DbItems.DbItemsNames.COLUMN_COUNT);
                isThere = cursChecker.getString(codeColnumIndex);
                if(!strGettedCode.equals("")){
                    etTakeCode.setEnabled(false);
                    etTakeName.setEnabled(false);
                }
                else {

                }
                etTakeName.setText(nameInBase);
                etTakeName.setEnabled(false);

            }
        }
        else{
            etTakeName.setEnabled(true);
            etTakeName.setClickable(true);
            etTakeName.setText("");
            Snackbar.make(findViewById(R.id.activityTakeId), getString(R.string.strIsntInBase), Snackbar.LENGTH_LONG).show();
        }
        cursChecker.close();
    }

    public boolean checkInBase(String textFromtvCode){
        String[] projection = new String[] {countColumnName, codeColumnName};
        cursChecker = dbTake.query(tableName, projection, codeColumnName+ "=?",new String[]{textFromtvCode} ,null, null, null);

        if(cursChecker.getCount()>0) {
            return true;
        }
        else {
            return false;
        }

    }
    public void clearViews(){
        etTakeName.setText("");
        etTakeCode.setText("");
        etTakeCount.setText("");
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        String strCodeCompare = etTakeCode.getText().toString();
        if(checkInBase(strCodeCompare)) {
            cursor = dbTake.query(DbItems.DbItemsNames.TABLE_NAME, projection, strCode + "=?", new String[]{strCodeCompare}, null, null, null);
            while (cursor.moveToNext()) {
                setNames();
                tvArka.setText(getString(R.string.countInBase) + isThere);
            }
        }
        else {
            setNames();
        }
    }
}

package com.artipd.tigran.pahest;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.artipd.tigran.pahest.Data.DataBase;
import com.artipd.tigran.pahest.Data.DbItems;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class ActivityAdd extends AppCompatActivity implements View.OnClickListener, View.OnFocusChangeListener {

    Button btnAddItem, btnScanCode;
    EditText etCodeAdd, etNameAdd, etCountAdd, etcoastAdd, etLimit;
    public static final int REQUEST_CODE = 0x0000c0de;

    SQLiteDatabase dbAdd;
    DataBase dbHelper;
    Cursor cursChecker;
    //Anunner@ talisenq popoxakannerin
    public static final String tableName = DbItems.DbItemsNames.TABLE_NAME;
    public static final String codeColumnName = DbItems.DbItemsNames.COLUMN_CODE;
    public static final String nameColnumName = DbItems.DbItemsNames.COLUMN_NAME;
    public static final String countColumnName = DbItems.DbItemsNames.COLUMN_COUNT;
    public static final String coastColnumName = DbItems.DbItemsNames.COLUMN_COAST;
    public static final String limitColnumName = DbItems.DbItemsNames.COLUMN_LIMIT;
    TextView tvInBase;
    final String[] projectionWithItemName = new String[]{nameColnumName, coastColnumName, countColumnName, limitColnumName};
    Boolean isFromListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        String strGettedCode = "";
        //Stugumenq user@ vortexice mtel mer Activity
        //ete extra chka uremn miangamic bacvel e AddActivitin
        if (getIntent().getExtras() == null) {
            isFromListView = false;

        //Ete extra ka stanum enq dranq
        } else {
            strGettedCode = getIntent().getExtras().getString("CodeFromBase");
            isFromListView = getIntent().getExtras().getBoolean("isFromListView");
        }


        //Stanum enq fieldneri id
        etCodeAdd = findViewById(R.id.editText_code_number);
        etcoastAdd = findViewById(R.id.editText_coast);
        etCountAdd = findViewById(R.id.editText_count);
        etNameAdd = findViewById(R.id.editText_name);
        btnScanCode = findViewById(R.id.btn_scan_code);
        btnAddItem = findViewById(R.id.buttonAdd);
        tvInBase = findViewById(R.id.tvInBase);
        etLimit = findViewById(R.id.editText_limit);
        dbHelper = new DataBase(this);
        dbAdd = dbHelper.getWritableDatabase();
        //if (!strGettedCode.equals("")) {
        //ete Activitin bacvel e listVCiewic apa stanumenq ekac text@ u texadrum etCodeAddi mej
        if (isFromListView) {
            etCodeAdd.setText(strGettedCode);
        }
        //stexcum enq onfocusChangeListener vorpeszi changei jamanak lracnenq hamapatasxan fieldner@
        etCodeAdd.setOnFocusChangeListener(this);
        btnAddItem.setOnClickListener(this);
        //avelacnum enq onClickListener barkodi scaneri miacman hamar
        btnScanCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new IntentIntegrator(ActivityAdd.this).setOrientationLocked(false).setCaptureActivity(CustomScannerActivity.class).initiateScan();
            }
        });
    }


    //Mshakum enq add kam update button-i onclick@
    @Override
    public void onClick(View view) {

        int addToCode;
        //stexcum enq contentValues vorpeszi lcnenq u grenq bazai mej
        ContentValues cv = new ContentValues();

        //Stugum enq, ete bolor fieldner@ lracvac en sharunakum enq kod@
        if (checkIsAllFilled()) {
            //stanum enq etCodei arjeq@ ev stugum ardyoq ka bazayum te voch?
            if (checkInBase(etCodeAdd.getText().toString())) {
                //cursChecker@ stanum e ir arjeq@ setNames funkciaium vorn el ir hertin kanchvum e onFocusChangeum
                //cursChecker = dbAdd.query(tableName, projectionWithItemName, codeColumnName + "=?", new String[]{codeFromEditText}, null, null, null, null);

                while (cursChecker.moveToNext()) {
                    //bazayum arkayutyan depqum apranqi qanakutyan pahpanman fieldi index@
                  //  int cursCountInBaseIndex = cursChecker.getColumnIndex(countColumnName); stanum e index@
                    int countInBase = cursChecker.getInt(cursChecker.getColumnIndex(countColumnName));

                    //Stugum enq ete activitin bacvel e list viewic
                    if (isFromListView) {

                        //addToCoden stanum e etCountAdd-i arjeq@ qani vor ayn petq e tarmacvi vochte avelacvi kam pakasecvi
                        addToCode = Integer.valueOf(etCountAdd.getText().toString());

                        //texadrum enq hamapatasxan fielderi arjeqner@ ContentValuesi mej
                        //kardum enq bolor fielder@ update anelu hamar
                        cv.put(countColumnName, addToCode);
                        cv.put(codeColumnName, etCodeAdd.getText().toString());
                        cv.put(nameColnumName, etNameAdd.getText().toString());
                        cv.put(coastColnumName, Double.valueOf(etcoastAdd.getText().toString()));
                        cv.put(limitColnumName, Integer.valueOf(etLimit.getText().toString()));
                        super.onBackPressed();
                        //ete voch apa addToCoden stanum e bazayum exac arjeqin + fieldum grac arjeq@
                    } else {
                        addToCode = countInBase + Integer.valueOf(etCountAdd.getText().toString());
                        //arjeq@ texadrum enq content valuesi mej
                        cv.put(countColumnName, addToCode);
                    }
                    cv.put(limitColnumName, Integer.valueOf(etLimit.getText().toString()));
                    //Bazayi tablen updateenq anum u snachbarov cucadrum enq
                    dbAdd.update(tableName, cv, codeColumnName + "=?", new String[]{etCodeAdd.getText().toString()});
                    Snackbar.make(findViewById(R.id.activityAddId), getString(R.string.strUpdated), Snackbar.LENGTH_SHORT).show();
                }
            //Depq erb arjeq@ bazayum chka
            } else {
                //stanum enq addToCodei arjeq@ ayn bazayum texadrelu hamar
                //Myus filderi arjeqner@ texadrum enq content valuesi mej
                if(etcoastAdd.getText().toString().equals("")){
                    etcoastAdd.setText("0");
                }
                if(etLimit.getText().toString().equals("")){
                    etLimit.setText("0");
                }

                addToCode = Integer.valueOf(etCountAdd.getText().toString());
                cv.put(codeColumnName, etCodeAdd.getText().toString());
                cv.put(nameColnumName, etNameAdd.getText().toString());
                cv.put(coastColnumName, Double.valueOf(etcoastAdd.getText().toString()));
                cv.put(countColumnName, addToCode);
                cv.put(limitColnumName, Integer.valueOf(etLimit.getText().toString()));
                //grancum enq content values@ bazayum ev cucadrum enq hamapatasxan grarum@ snackbarov
                dbAdd.insert(tableName, null, cv);
                Snackbar.make(view, getString(R.string.strAdded), Snackbar.LENGTH_SHORT).show();


            }
            //avelacneluc kam popoxeluc heto maqrum enq fieldner@
            //stugum enq, ete list viewic e bacvel activitin
            //kanchum enq onBackPressed@
            clearViews();
           // if (isFromListView) {
           //     super.onBackPressed();
           // }

        //Ete voch bolor fieldern en lracvac userin asum enq vor lracni dranq
        } else {
            Snackbar.make(view, getString(R.string.fill_all_fields), Snackbar.LENGTH_LONG).show();
        }

    }

    //Stugum ev veradarcnum ardyoq cod@ bazayum ka te voch
    public boolean checkInBase(String textFromtvCode) {
        String[] projection = new String[]{countColumnName, codeColumnName};
        cursChecker = dbAdd.query(tableName, projection, codeColumnName + "=?", new String[]{textFromtvCode}, null, null, null);
        return cursChecker.getCount() > 0;

    }

    //Lracnum enq fieldneri arjeqner@
    public void setNames() {
        //Stanum enq apranqi kodi arjeq@
        String codeFromEditText = etCodeAdd.getText().toString();
        //ete cod@ bazayum ka
        if (checkInBase(codeFromEditText)) {
            //Stexcum enq cursorChecker@ mej@ lcnelov hamapatasxan kodov arjeqner@
            cursChecker = dbAdd.query(tableName, projectionWithItemName, codeColumnName + "=?", new String[]{codeFromEditText}, null, null, null, null);
            while (cursChecker.moveToNext()) {
                //int countInBaseColInd = cursChecker.getColumnIndex(countColumnName);
                //int nameColnumIndex = cursChecker.getColumnIndex(nameColnumName);
                //int coastColnumIndex = cursChecker.getColumnIndex(coastColnumName);

                //Cursoric stanum enq Columneri arjeqner@ ev texadrum hamapatasxan fieldnerum
                int countInBase = cursChecker.getInt(cursChecker.getColumnIndex(countColumnName));
                String nameInBase = cursChecker.getString(cursChecker.getColumnIndex(nameColnumName));
                Double coastInBase = cursChecker.getDouble(cursChecker.getColumnIndex(coastColnumName));
                int limitInBase = cursChecker.getInt(cursChecker.getColumnIndex(limitColnumName));
                etNameAdd.setText(nameInBase);
                etcoastAdd.setText(String.valueOf(coastInBase));
                tvInBase.setText(getString(R.string.countInBase) + String.valueOf(countInBase));
                etLimit.setText(String.valueOf(limitInBase));
                //Ete activitin bacvel e List viewic
                //Argelapakum enq Codi field@ ev bacum mnacac@
                if (isFromListView) {
                    etNameAdd.setEnabled(true);
                    etcoastAdd.setEnabled(true);
                    etCountAdd.setText(String.valueOf(countInBase));
                    btnAddItem.setText(getString(R.string.btn_text_update));
                    etCountAdd.setEnabled(true);
                    etCodeAdd.setEnabled(false);
                //Hakarak depqum kaskanum enq vor tvyal@ bazayum ka ev anhrajesht e miayn avelacnel dra qanakutyun@
                } else {
                    etNameAdd.setEnabled(false);
                    etcoastAdd.setEnabled(false);
                    btnAddItem.setText(getString(R.string.btn_text_update));
                }
            }

        //ete kod@ bazayum chka fieldner@ maqrum enq
        } else {
            Snackbar.make(findViewById(R.id.activityAddId), getString(R.string.strIsntInBase), Snackbar.LENGTH_SHORT).show();
            tvInBase.setText("");
            etNameAdd.setEnabled(true);
            etcoastAdd.setEnabled(true);
            etNameAdd.setText("");
            etcoastAdd.setText("");
            etLimit.setText("");
            btnAddItem.setText(getString(R.string.text_btn_add));

        }

    }

    //Kanchvum e erb scan enq anum shtrixkod@
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanningResult;

        if (requestCode == REQUEST_CODE) {
            scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, R.string.strNothingScanned, Toast.LENGTH_LONG).show();
            } else {
                //ete scan@ hajox e ancel lracnum enq ardyunq@
                if (scanningResult != null) {
                    etCodeAdd.setText(scanningResult.getContents());
                    setNames();
                }
            }
        }


    }

    public void clearViews() {
        etCodeAdd.setText("");
        etCountAdd.setText("");
        etNameAdd.setText("");
        etcoastAdd.setText("");
        etLimit.setText("");
    }


    @Override
    //erb fieldic focus@ poxvum e, setNamesnenq kanchum
    //naxoroq stugelov, vorpeszi codi field@ datark chlini
    public void onFocusChange(View v, boolean hasFocus) {
        if (!etCodeAdd.getText().toString().equals("")) {
            setNames();
        }
    }

    //Stugum enq ardyoq bolor fieldern en lracvac te voch
    public boolean checkIsAllFilled() {

        return !etNameAdd.getText().toString().equals("") && !etCodeAdd.getText().toString().equals("")
                && !etCountAdd.getText().toString().equals("");
    }
}

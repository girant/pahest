package com.artipd.tigran.pahest;

/**
 * Created by tigran on 12/26/17.
 */

public class ListItemModelClass {
    String name;
    String code;
    int count, id;
    Double coast;

    public ListItemModelClass(String _name, String _code, int _count, Double _coast) {
        name = _name;
        code = _code;
        count = _count;
        coast = _coast;
    }
}

package com.artipd.tigran.pahest;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.PopupMenu;
import android.widget.TextView;


import java.util.List;

public class AdapterRecyclerView extends RecyclerView.Adapter<AdapterRecyclerView.RecyclerViewHolder> implements PopupMenu.OnMenuItemClickListener {
    int lastPosition = -1;

    private List<ListItemModelClass> viewModel;

    public AdapterRecyclerView(List<ListItemModelClass> viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.to_inflate_to_recycler, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        ListItemModelClass fbModelAtPosition = viewModel.get(position);
        final Resources resources = holder.itemView.getResources();
        holder.tvName.setText(fbModelAtPosition.name);
        //holder.tvCode.setText(holder.itemView.getResources().getString(R.string.hint_number) + ": " + fbModelAtPosition.code);
        holder.tvCount.setText(holder.itemView.getResources().getString(R.string.hint_count) + ": " + fbModelAtPosition.count + " ");
        holder.tvCoast.setText(holder.itemView.getResources().getString(R.string.hint_coast) + ": " + String.valueOf(fbModelAtPosition.coast));
        View v = holder.itemView.findViewById(R.id.relLayoutInflated);
        v.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            @Override
            public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
                menu.add(holder.getAdapterPosition(), 0, 0, resources.getString(R.string.delete_context_menu));
                menu.add(holder.getAdapterPosition(), 1, 0, resources.getString(R.string.edit_context_menu));
                menu.add(holder.getAdapterPosition(), 2, 0, resources.getString(R.string.take_context_menu));

            }
        });

    /*    View v = holder.itemView.findViewById(R.id.relLayoutInflated);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
                popupMenu.inflate(R.menu.popupmenu);
                popupMenu.setOnMenuItemClickListener(AdapterRecyclerView.this);
                popupMenu.show();
            }
        });
*/
        int animId;
        if (position > lastPosition) {
            animId = R.anim.up_from_bottom;

        } else {
            animId = R.anim.down_from_top;

        }
        lastPosition = position;
        Animation animation = AnimationUtils.loadAnimation(holder.itemView.getContext(), animId);
        holder.itemView.startAnimation(animation);
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public int getItemCount() {
        return viewModel.size();
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {

        Log.d("ITEM SELECTED", "ITEM SELECTED");
        return false;
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvCode, tvCount, tvCoast;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvFbName);
            //tvCode = itemView.findViewById(R.id.tvFbCode);
            tvCoast = itemView.findViewById(R.id.tvFbCoast);
            tvCount = itemView.findViewById(R.id.tvFbCount);
        }
    }

}
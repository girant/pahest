package com.artipd.tigran.pahest;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;
import android.widget.Toast;

import com.artipd.tigran.pahest.Data.DataBase;
import com.artipd.tigran.pahest.Data.DbItems;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private RecyclerView recView;
    private List<ListItemModelClass> model;
    private AdapterRecyclerView adapter;
    FloatingActionsMenu floatMenu;
    boolean underLimitIsChecked;

    //create projection to work with informaation from DB
    final String [] projection = new String[]{DbItems.DbItemsNames._ID, DbItems.DbItemsNames.COLUMN_NAME,
            DbItems.DbItemsNames.COLUMN_COAST, DbItems.DbItemsNames.COLUMN_COUNT, DbItems.DbItemsNames.COLUMN_CODE};

    //Declare cursors
    Cursor cursAdapted, cursorUnderLimit;
    SQLiteDatabase dbList;
    DataBase dbHelper;
    Intent intAddActivity;

    //Declare request code for editing entry from list
    public static final int REQUEST_EDIT = 2;


    //Method to create popup menu while long press on the item
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case 0:
               deleteFromBase(item.getGroupId());
                break;
            case 1:
                EditEntry(item.getGroupId());
                break;
            case 2:
                takeFromBase(item.getGroupId());
            default: break;
        }
        return super.onContextItemSelected(item);

    }




    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recView = findViewById(R.id.recyclerWithHolder);
        model = new ArrayList<>();
        recView.setHasFixedSize(true);
        adapter = new AdapterRecyclerView(model);
        dbHelper = new DataBase(this);
        dbList = dbHelper.getReadableDatabase();
        cursAdapted = dbList.query(DbItems.DbItemsNames.TABLE_NAME, projection, null, null, null, null, "name COLLATE NOCASE ASC");
        cursorUnderLimit = dbList.query(DbItems.DbItemsNames.TABLE_NAME, projection, DbItems.DbItemsNames.COLUMN_LIMIT +">" + DbItems.DbItemsNames.COLUMN_COUNT, null, null, null, "name COLLATE NOCASE ASC");
        recView.setHasFixedSize(true);
        LinearLayoutManager linLayManager = new LinearLayoutManager(this);
        linLayManager.setOrientation(LinearLayoutManager.VERTICAL);
        recView.setLayoutManager(linLayManager);
        floatMenu = findViewById(R.id.fabMenu);
        final com.getbase.floatingactionbutton.FloatingActionButton fabAdd = findViewById(R.id.fabAdd);
        final com.getbase.floatingactionbutton.FloatingActionButton fabTake = findViewById(R.id.fabTake);
        fabTake.setOnClickListener(this);
        fabAdd.setOnClickListener(this);

        filldata(cursAdapted);
        recView.setAdapter(adapter);

        recView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(floatMenu.isExpanded()){
                    floatMenu.collapse();
                }
                return false;
            }
        });


        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        recView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(floatMenu.isExpanded()){
                    floatMenu.collapse();
                }
                super.onScrolled(recyclerView, dx, dy);
                if(dy>0){
                    if (floatMenu.isShown()){

                        floatMenu.setVisibility(View.INVISIBLE);
                    }
             }
             else floatMenu.setVisibility(View.VISIBLE);
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fabAdd:
                intAddActivity = new Intent(this, ActivityAdd.class);
                startActivityForResult(intAddActivity, REQUEST_EDIT);
                break;
            case R.id.fabTake:
                intAddActivity = new Intent(this, TakeActivity.class);
                startActivityForResult(intAddActivity, REQUEST_EDIT);
                break;
            default:break;
        }

    }

    private void filldata(Cursor cursor) {
        model.clear();
        cursor.moveToFirst();
        try {
            do {
                String name = cursor.getString(cursor.getColumnIndex(DbItems.DbItemsNames.COLUMN_NAME));
                String code = cursor.getString(cursor.getColumnIndex(DbItems.DbItemsNames.COLUMN_CODE));
                int count = cursor.getInt(cursor.getColumnIndex(DbItems.DbItemsNames.COLUMN_COUNT));
                Double coast = cursor.getDouble(cursor.getColumnIndex(DbItems.DbItemsNames.COLUMN_COAST));

                model.add(new ListItemModelClass(name, code, count, coast));
                adapter.notifyDataSetChanged();
            }
            while (cursor.moveToNext());
        }
        catch (IndexOutOfBoundsException e){
            Toast.makeText(this, "Nothing to show", Toast.LENGTH_SHORT).show();
        }
    }


    private void filldata(String searchString){
        model.clear();
        Cursor curs;
        if(!underLimitIsChecked) {
            curs = dbList.rawQuery("select * from " + DbItems.DbItemsNames.TABLE_NAME + " where " +
                    DbItems.DbItemsNames.COLUMN_NAME + " LIKE ? ORDER BY ?", new String[]{"%" + searchString + "%", "name COLLATE NOCASE ASC"});
        }
        else {
            curs = dbList.rawQuery("select * from " + DbItems.DbItemsNames.TABLE_NAME + " where " +
                    DbItems.DbItemsNames.COLUMN_LIMIT + ">" + DbItems.DbItemsNames.COLUMN_COUNT +" AND "+
                    DbItems.DbItemsNames.COLUMN_NAME + " LIKE ? ORDER BY ? ", new String[]{"%" + searchString + "%", "name COLLATE NOCASE ASC"});
        }
        if(curs.getCount()>0) {
            curs.moveToFirst();
            do {
                String name = curs.getString(curs.getColumnIndex(DbItems.DbItemsNames.COLUMN_NAME));
                String code = curs.getString(curs.getColumnIndex(DbItems.DbItemsNames.COLUMN_CODE));
                int count = curs.getInt(curs.getColumnIndex(DbItems.DbItemsNames.COLUMN_COUNT));
                Double coast = curs.getDouble(curs.getColumnIndex(DbItems.DbItemsNames.COLUMN_COAST));

                model.add(new ListItemModelClass(name, code, count, coast));
            } while (curs.moveToNext());
        }
        curs.close();
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_item_search);
        android.widget.SearchView searchView = (android.widget.SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filldata(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        switch(id){
            case R.id.action_settings:
                return true;
            case R.id.show_only_underlimit:
                item.setChecked(!item.isChecked());

                if(item.isChecked()){
                    filldata(cursorUnderLimit);
                    underLimitIsChecked = true;
                }
                else {
                    filldata(cursAdapted);
                    underLimitIsChecked = false;
                }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_general) {
            Intent intListActiv = new Intent(this, MainActivity.class);
            startActivity(intListActiv);
        } else if (id == R.id.nav_add) {
            intAddActivity = new Intent(this, ActivityAdd.class);
            startActivity(intAddActivity);

        } else if (id == R.id.nav_take) {
            Intent intTakeActivity = new Intent(MainActivity.this, TakeActivity.class);
            startActivity(intTakeActivity);

        } else if (id == R.id.nav_view) {

        } else if (id == R.id.nav_limits){

        }


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(floatMenu.isExpanded()){
            floatMenu.collapse();
        }
    }

    public void deleteFromBase(int itemId){
        dbList.delete(DbItems.DbItemsNames.TABLE_NAME, DbItems.DbItemsNames.COLUMN_CODE + "=?",new String[]{model.get(itemId).code});
        model.remove(itemId);
        adapter.notifyDataSetChanged();
    }

    private void EditEntry(int itemId ) {
        final Boolean fromBase = true;
        Intent intEditElement = new Intent(getApplicationContext(), ActivityAdd.class);
        String extra = model.get(itemId).code;
        intEditElement.putExtra("CodeFromBase", extra);
        intEditElement.putExtra("isFromListView", fromBase);
        startActivityForResult(intEditElement, REQUEST_EDIT);

    }

    private void takeFromBase(int itemId) {
        String extra = model.get(itemId).code;
        final Boolean fromBase = true;
        Intent intTakeElement = new Intent(getApplicationContext(), TakeActivity.class);
        intTakeElement.putExtra("CodeFromBase", extra);
        intTakeElement.putExtra("isFromListView", fromBase);
        startActivityForResult(intTakeElement, REQUEST_EDIT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_EDIT){
            finish();
            startActivity(getIntent());
        }
    }
}

